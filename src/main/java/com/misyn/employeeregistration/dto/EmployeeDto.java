package com.misyn.employeeregistration.dto;

import com.misyn.employeeregistration.entity.Employee;

import java.io.Serializable;

/**
 * @author Thanura
 */
public class EmployeeDto implements Serializable {
    private Integer id;
    private String name;
    private String address;
    private String nic;
    private Integer age;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Employee toEntity() {
        Employee employee = new Employee();
        employee.setName(this.name);
        employee.setAddress(this.address);
        employee.setNic(this.nic);
        employee.setAge(this.age);
        return employee;
    }
}
