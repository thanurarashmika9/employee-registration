package com.misyn.employeeregistration.service.impl;

import com.misyn.employeeregistration.dto.EmployeeDto;
import com.misyn.employeeregistration.entity.Employee;
import com.misyn.employeeregistration.repository.EmployeeDetailRepository;
import com.misyn.employeeregistration.service.EmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Thanura
 */

@Service
public class EmployeeServiceImpl implements EmployeeService {
    private final Logger LOGGER = LoggerFactory.getLogger(EmployeeServiceImpl.class);

    @Autowired
    private EmployeeDetailRepository employeeDetailRepository;

    @Override
    public EmployeeDto saveEmployee(EmployeeDto employeeDto) throws Exception {
        EmployeeDto employee;
        try {
            Employee savedEmployee = employeeDetailRepository.save(employeeDto.toEntity());
            employee = savedEmployee.toDto();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw e;
        }
        return employee;
    }
}
