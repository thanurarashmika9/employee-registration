package com.misyn.employeeregistration.service;

import com.misyn.employeeregistration.dto.EmployeeDto;

/**
 * @author Thanura
 */
public interface EmployeeService {
    EmployeeDto saveEmployee(EmployeeDto employeeDto) throws Exception;
}
