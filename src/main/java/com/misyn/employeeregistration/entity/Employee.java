package com.misyn.employeeregistration.entity;

import com.misyn.employeeregistration.dto.EmployeeDto;

import javax.persistence.*;

/**
 * @author Thanura
 */
@Entity
@Table(name = "employee_detail")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private String address;
    private String nic;
    private Integer age;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public EmployeeDto toDto() {
        EmployeeDto employeeDto = new EmployeeDto();
        employeeDto.setId(this.id);
        employeeDto.setName(this.name);
        employeeDto.setAddress(this.address);
        employeeDto.setNic(this.nic);
        employeeDto.setAge(this.age);
        return employeeDto;
    }
}
