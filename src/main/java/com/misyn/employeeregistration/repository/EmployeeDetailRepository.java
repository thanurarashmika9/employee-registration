package com.misyn.employeeregistration.repository;

import com.misyn.employeeregistration.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Thanura
 */
public interface EmployeeDetailRepository extends JpaRepository<Employee, Integer> {
}
